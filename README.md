# SDP Buffer Proof of Concept

This repository shows how a prototype implementation of the SDP Buffer
could be achieved by using Helm to deploy Kubernetes persistent volume
claims. This fits with the current design of the SDP prototype which
uses Helm to deploy containers to do the processing.

There are three Helm charts:
- `create-buffer`: Creates a buffer reservation using a persistent
  volume claim.
- `write-to-buffer`: Creates a job to write a string to a file in the
  buffer reservation.
- `read-from-buffer`: Creates a job to print the contents of a file in
  the buffer reservation.

Note that these charts are written for Helm version 3.

## Creating the buffer reservation

Install the `create-buffer` chart, for example, with the release name
`buffer-test`:

    $ helm install buffer-test create-buffer

The persistent volume claim (PVC) and the associated persistent volume,
which has been created dynamically to satisfy the claim, can be shown
using:

    $ kubectl get pvc,pv
    NAME                                STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
    persistentvolumeclaim/buffer-test   Bound    pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a   1Gi        RWO            standard       15s
    
    NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                 STORAGECLASS   REASON   AGE
    persistentvolume/pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a   1Gi        RWO            Delete           Bound    default/buffer-test   standard                14s

Note the name of the PVC is the same as the Helm chart release name. To
find the location of the persistent volume in the filesystem, do:

    $ kubectl describe pv pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a
    Name:            pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a
    Labels:          <none>
    Annotations:     hostPathProvisionerIdentity: e0e8fb55-11d6-11ea-9412-f6aef5d00296
                     pv.kubernetes.io/provisioned-by: k8s.io/minikube-hostpath
    Finalizers:      [kubernetes.io/pv-protection]
    StorageClass:    standard
    Status:          Bound
    Claim:           default/buffer-test
    Reclaim Policy:  Delete
    Access Modes:    RWO
    VolumeMode:      Filesystem
    Capacity:        1Gi
    Node Affinity:   <none>
    Message:         
    Source:
        Type:          HostPath (bare host directory volume)
        Path:          /tmp/hostpath-provisioner/pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a
        HostPathType:  
    Events:            <none>

The size of the buffer is 1 GiB by default, but this can be changed by
setting the `buffer.size` value when installing the chart, for example:

    $ helm install buffer-test create-buffer --set buffer.size=2Gi

## Writing to the buffer reservation

Install the `write-to-buffer` chart:

    $ helm install write-test write-to-buffer

If your PVC is named something other than `buffer-test`, then you should
specify that by setting the `buffer.name` value when installing the
chart:

    $ helm install write-test write-to-buffer --set buffer.name=<pvc_name>

This chart runs a job which writes the string `foobar` to the file
`test` in the buffer reservation (the string can be changed by setting
the `buffer.string` value and the filename by setting the `buffer.file`
value). The job should finish very quickly:

    $ kubectl get pod
    NAME               READY   STATUS      RESTARTS   AGE
    write-test-nf4q8   0/1     Completed   0          12s

After that, you can uninstall the chart release:

    $ helm uninstall write-test

## Examining the contents of the buffer reservation

You can examine the contents of the PVC by looking at the files directly
in the filesystem (if you are using minikube you need to log in to the
virtual machine first by doing `minikube ssh`):

    $ ls /tmp/hostpath-provisioner/pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a
    test
    $ cat /tmp/hostpath-provisioner/pvc-7c1e8bb6-21fe-4c9b-b022-812864cdff2a/test
    foobar

## Reading from the buffer reservation

Install the `read-from-buffer` chart:

    $ helm install read-test read-from-buffer

As with the writing stage, if your PVC is named something other than
`buffer-test`, then you should specify that by setting the `buffer.name`
value when installing the chart:

    $ helm install read-test read-from-buffer --set buffer.name=<pvc_name>

This chart runs a job which prints the contents of the file `test` in
the buffer reservation (the filename can be changed by setting the
`buffer.file` value). Again the job should finish quickly:

    $ kubectl get pod
    NAME              READY   STATUS      RESTARTS   AGE
    read-test-lv7jz   0/1     Completed   0          4s

The output from the file can be found in the logs of the pod:

    $ kubectl logs read-test-lv7jz
    foobar

After that, you can uninstall the chart release:

    $ helm uninstall read-test

## Removing the buffer reservation

The buffer reservation is removed by uninstalling the `create-buffer`
chart release:

    $ helm uninstall buffer-test

You can confirm that the PVC and its associated persistent volume have
been removed by doing:

    $ kubectl get pvc,pv
    No resources found in default namespace.
